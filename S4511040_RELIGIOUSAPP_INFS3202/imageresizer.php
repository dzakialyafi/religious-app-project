<?php

function img_resize($target, $newcopy, $w, $h, $ext) {
	list($w_orig, $h_orig) = getimagesize($target);
	$scala_ratio = $w_orig / $h_orig;
	if (($w / $h) > $scala_ratio) {
			$w = $h * $scala_ratio;

	}else{
			$h = $w / $scala_ratio;
	}
	$img = "";
	$ext = strtolower($ext);
	if($ext == "gif"){
		$img = imagecreatefromgif($target);

		}else if($ext == "png"){
		$img = imagecreatefrompng($target);
		}else{
		$img = imagecreatefromjpeg($target);
		}
		$variable = imagecreatetruecolor($w, $h);

		imagecopyresampled($variable, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
		imagejpeg($variable, $newcopy, 80);
}
?>