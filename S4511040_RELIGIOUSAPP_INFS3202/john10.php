<?php
session_start();
include ('connect.php');
if(@$_SESSION['user_name']){
  if(@$_GET['action'] == 'logout'){
        session_destroy();
        header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Holy Book</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->

<style>
.nav-pills > li > a.active {
  background-color: #340759 !important;
  color: #fff !important;
}

.nav-pills > li > a:hover {
  color: #98B9F2 !important;
}

.nav-link-color {
  color: #340759;
}
</style>

  </head>

<body>
  <?php include("header.php"); ?>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
          <ul class="nav nav-pills flex-column">
            <h2>James</h2>
            <li class="nav-item">
              <a class="nav-link" href="james1.php">Chapter 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="james2.php">Chapter 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="james3.php">Chapter 3</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="james4.php">Chapter 4</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="james5.php">Chapter 5</a>
            </li>

          </ul>

          <ul class="nav nav-pills flex-column">
            <h2>John</h2>
            <li class="nav-item">
              <a class="nav-link" href="john1.php">Chapter 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john2.php">Chapter 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john3.php">Chapter 3</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john4.php">Chapter 4</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john5.php">Chapter 5</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john6.php">Chapter 6</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john7.php">Chapter 7</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john8.php">Chapter 8</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john9.php">Chapter 9</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="john10.php">Chapter 10 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john11.php">Chapter 11</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john12.php">Chapter 12</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john13.php">Chapter 13</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john14.php">Chapter 14</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="john15.php">Chapter 15</a>
            </li>
          </ul>

        </nav>

        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
          <center><h1>Holy Book</h1></center>

          <div class="table-responsive">

            <div class="container">
              <div class="quran-table">
                <table class="table table-bordered table-striped" id="quran_table">
                  <h1 id="bookname" align="center"></h1>
                  <h1 id="chapter" align="center"></h1>
                  <tr>
                    <th>Verse</th>
                    <th>Recitation</th>
                  </tr>
                </table>
              </div>
            </div>

          </div>
        </main>
      </div>
    </div>
    <script>
          $(document).ready(function(){
            
            $.getJSON("https://bertodaulat.000webhostapp.com/john.json", function(data){
              var quran_data = '';
              var i = 1;
              document.getElementById("chapter").innerHTML = "Chapter "+ data.book[10].chapter_nr;
              document.getElementById("bookname").innerHTML = data.book_name;
              var data = data.book[10].chapter;
              $.each(data, function(key, value){
                quran_data += '<tr>';
                quran_data += '<td>' + String(i) + '</td>'+'<td>'+ data[i]['verse'] + '</td>';
                quran_data += '</tr>';
                i += 1;
              });
              $('#quran_table').append(quran_data);
              console.log(data);
            });
          });

        </script>

</body>
</html>
<?php
}else{
    echo 'You have logged out';
  }
?>