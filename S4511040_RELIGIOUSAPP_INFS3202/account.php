<?php
	session_start();
	include ('connect.php');
	if(@$_SESSION["user_name"]){
		if(@$_GET['action'] == "logout"){
			session_destroy();
			header("Location: login.php");
		}

		if(@$_GET['action'] == 'cp'){
			header("Location: change_password.php");
		}

		if(@$_GET['action'] == 'da'){
			header("Location: delete_account.php");
		}
?>
<html>
<head>
<title>Religious App</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">

	<!-- Bootstrap core CSS -->
	<link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="https://v4-alpha.getbootstrap.com/examples/carousel/carousel.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
	<title>Religious App</title>

	<style>

		.card {
		  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
		  max-width: 700px;
		  margin: auto;
		  text-align: center;
		  font-family: arial;
		}
		button {
			width: 300px;
		}

		body {
			background-color: #E8F0ED;
		}
	</style>

</head>

<body>
	<?php include("header.php"); ?>
	<?php 
	$sql = "SELECT * FROM db_user WHERE user_name = '".$_SESSION['user_name']."'";
	$check = mysqli_query($connect, $sql);
	$rows = mysqli_num_rows($check);
	while($row = mysqli_fetch_assoc($check)){
		$user_name = $row['user_name'];
		$user_email = $row['user_email'];
		$user_id = $row['user_id'];
		$user_image = $row['image'];
		$user_date = $row['date_registered'];
	}
	?>
	<br />		<br><br>
				<h2 style="text-align:center">User Profile Card</h2>
				<br>
					<div class="card">
							<br>
							<center>
							<?php echo "<img src='$user_image' class='rounded-circle' height=200px width=200px>" ?> 
							</center>
							<br>
							<br>
							<h3 class="profilename"><?php echo $user_name; ?> </h3><br>
							<span class="profileemail">Email: <?php echo $user_email; ?></span><br>
							<span class="profileid">User ID: <?php echo $user_id; ?></span> <br>
							<span classs="dateregis">Date Registered: <?php echo $user_date; ?></span> <br>	
							<br><br>

								
									<a href='account.php?action=cp'><button class="btn btn-outline-primary">Change Password</button></a><br>
									<a href='account.php?action=ci'><button class="btn btn-outline-primary">Change Image</button></a><br>
									<a href='account.php?action=da'><button class="btn btn-outline-danger">Delete Account</button></a><br>
								
					</div>
</body>
</html>

<?php

	if(@$_GET['action'] == 'ci'){
		echo "<center>";
		echo "<form action='account.php?action=ci' method='POST' enctype='multipart/form-data'><br />
		Available file extension: <b>.PNG .JPG .JPEG</b><br /><br />
		<input type='file' name='image'><br />
		<input type='submit' name='change_image' value='change'><br />";

		if(isset($_POST['change_image'])){
			$errors = array();
			$allowed_ext = array('png', 'jpg', 'jpeg');

			$filename = $_FILES['image']['name'];
			$file_ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
			$file_size = $_FILES['image']['size'];
			$file_tmp = $_FILES['image']['tmp_name'];

			if(in_array($file_ext, $allowed_ext) === false){
				$errors[] = 'This File Extension is not allowed';
			}

			if($file_size > 2097152){
				$errors[] = 'Image must be under 2MB';
			}

			if(empty($errors)){
				move_uploaded_file($file_tmp, 'images/'.$filename);
				$image_up = 'images/'.$filename; 
				$sql1 = "UPDATE db_user SET image='".$image_up."' WHERE user_name='".$_SESSION['user_name']."'";
				if($query = mysqli_query($connect, $sql1)){
					echo "Your Profile Image has changed";
				}

			}else{
				foreach ($errors as $error) {
					echo $error, "<br />";
				}
			}
		}
	}
}else{
		echo "You must be logged in";
	}
?>