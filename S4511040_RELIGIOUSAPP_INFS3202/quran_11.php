<?php
  session_start();
  include ('connect.php');
  if(@$_SESSION['user_name']){
    if(@$_GET['action'] == 'logout'){
          session_destroy();
          header('Location: login.php');
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Holy Book</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>

<style>
.nav-pills > li > a.active {
  background-color: #340759 !important;
  color: #fff !important;
}

.nav-pills > li > a:hover {
  color: #98B9F2 !important;
}

.nav-link-color {
  color: #340759;
}
</style>

  </head>

  <body>
    <?php include('header.php'); ?>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link" href="quran_1.php">Surah 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_2.php">Surah 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_3.php">Surah 3</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_4.php">Surah 4</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_5.php">Surah 5</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_6.php">Surah 6</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_7.php">Surah 7</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_8.php">Surah 8</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_9.php">Surah 9</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_10.php">Surah 10</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="quran_11.php">Surah 11 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_12.php">Surah 12</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_13.php">Surah 13</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_14.php">Surah 14</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_15.php">Surah 15</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_16.php">Surah 16</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_17.php">Surah 17</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_18.php">Surah 18</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_19.php">Surah 19</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quran_20.php">Surah 20</a>
            </li>
            
          </ul>

        </nav>

        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
          <h1>Holy Book</h1>

          <section class="row text-center placeholders">
            <div class="col-6 col-sm-3 placeholder">
              
            </div>
            <div class="col-6 col-sm-3 placeholder">
              <img src="allah.png" width="200" height="200" class="img1 img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              
            </div>
            <div class="col-6 col-sm-3 placeholder">
              <img src="muhammad.png" width="200" height="200" class="img2 img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              
            </div>
            <div class="col-6 col-sm-3 placeholder">
              
            </div>
          </section>

          <h2>Hud</h2>
          <div class="table-responsive">

              <div class="scene-element scene-element--fade-in-right">
                <div class="container">
                  <div class="quran-table" >
                    <table class="table table-bordered table-striped" id="quran_table">
                      <tr>
                        <br>
                        <br>
                        <th>Verse</th>
                        <th>Recitation</th>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>

          </div>
        </main>
      </div>
    </div>

        <script>
          jQuery(document).ready(function($){
            
            $.getJSON("https://bertodaulat.000webhostapp.com/surah_11.json", function(data){
              var quran_data = '';
              var i = 0;
              data = data[0];
              $.each(data, function(key, value){
                quran_data += '<tr>';
                quran_data += '<td>' + String(i) + '</td>'+'<td>'+ data[key] + '</td>';
                quran_data += '</tr>';
                i += 1;
              });
              $('#quran_table').append(quran_data);
            });
          });
        </script>   
  </body>
</html>
<?php
}else{
    echo 'You have logged out';
  }
?>
