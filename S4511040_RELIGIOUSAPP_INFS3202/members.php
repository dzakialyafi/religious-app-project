<?php
	session_start();
	include ('connect.php');
	if(@$_SESSION["user_name"]){
		if(@$_GET['action'] == "logout"){
			session_destroy();
			header("Location: login.php");
		}
?>
<html>
<head>
<title>Religious App</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">

	<!-- Bootstrap core CSS -->
	<link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="https://v4-alpha.getbootstrap.com/examples/carousel/carousel.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
	<title>Religious App</title>

	<style>

		.card {
		  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
		  max-width: 700px;
		  margin: auto;
		  text-align: center;
		  font-family: arial;
		}
		button {
			width: 300px;
		}

		body {
			background-color: #E8F0ED;
		}
	</style>

</head>

<body>
	<?php include("header.php"); ?>
	<br />
	<br />
	<h2 style="text-align:center">Member Details</h2>
	<?php 
		$sql = "SELECT * FROM db_user";
		$check = mysqli_query($connect, $sql);
		$rows = mysqli_num_rows($check);

		if(mysqli_num_rows($check) != 0){
			while($row = mysqli_fetch_assoc($check)){

				echo "
				<br>
					<div class='card'>
							<br>
							<center>
							<img src=".$row['image']." class='rounded-circle' height=200px width=200px>
							</center>
							<br>
							<br>
							<h3 class='profilename'> ".$row['user_name']." </h3><br>
							<span class='profileemail'>Email: ".$row['user_email']." </span><br>
							<span classs='dateregis'>Date Registered: ".$row['date_registered']." </span> <br>			
					</div>";
			}
		}
	?>
</body>
</html>

<?php
}else{
		echo "You must be logged in";
	}
?>