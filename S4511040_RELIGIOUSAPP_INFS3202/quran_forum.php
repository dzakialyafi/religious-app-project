<?php
	session_start();
	include ('connect.php');
	if(@$_SESSION["user_name"]){
			if(@$_GET['action'] == "logout"){
				session_destroy();
				header("Location: login.php");
	}
?>
<html>
<head>
	<title>Religious App</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
	<meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
	<meta name="author" content="PIXINVENT">
	<title>Bootstrap Cards - Stack Responsive Bootstrap 4 Admin Template</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
	  rel="stylesheet">
	<!-- BEGIN VENDOR CSS-->
	<link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/vendors.min.css">
	<!-- END VENDOR CSS-->
	<!-- BEGIN STACK CSS-->
	<link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/app.min.css">
	<!-- END STACK CSS-->
	<!-- BEGIN Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
	<link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/core/colors/palette-gradient.min.css">
	<!-- END Page Level CSS-->
	<!-- BEGIN Custom CSS-->
	<link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/assets/css/style.css">
	<!-- END Custom CSS-->
	<!-- Bootstrap core CSS -->
    <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
</head>

<body>
	<?php
	$post_content = @$_POST['post_content'];
	$post_date = date("Y/m/d");
	$select_user_id = "SELECT * FROM db_user WHERE user_name = '".$_SESSION['user_name']."'";
	$check = mysqli_query($connect, $select_user_id);
	$rows = mysqli_num_rows($check);
	while($row = mysqli_fetch_assoc($check)){
		$user_id = $row['user_id'];
	}

	if(isset($_POST['submit'])) {
		if($post_content){
			$sql1 = "INSERT INTO db_post_quran(post_id, post_content, post_date, post_by, user_id) VALUES(0,'".$post_content."','".$post_date."','".$_SESSION['user_name']."', '".$user_id."')";
			if(mysqli_query($connect, $sql1)){
				?>
				<div class="alert alert-success fade show" role="alert">
  				Succesfully Posted!
				</div>
				<?php
				header("Location: quran_forum.php");
			}else{
				?>
				<div class="alert alert-danger alert-dismissable fade show" role="alert">
  				Fail to post!
				</div>
				<?php
		}
		}else{
			echo "Please Fill in the Post Section";
		}
	}
?>
	<?php include("header.php"); ?>
	<br />
	<br />
	<br />
	<br />
	<h1 style="text-align: center;">Quran Forum</h1><br />
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
				<article class="card-body">
							<form method="POST" action="quran_forum.php">
				  				<div class="form-group">   
				  					<textarea style="resize: none; width: 655px; height: 200px;" name="post_content" class="form-control" ></textarea>
				  				</div>
					  			<div class="form-group">
					  				<button type="submit" name="submit" class="btn btn-outline-primary btn-block"> POST </button>
					  			</div>
							</form>
						</article>
					</div>
				</div>
			</div>
</body>
</html>

<?php
$sql2 = "SELECT * FROM db_post_quran";
	$check = mysqli_query($connect, $sql2);

	if(mysqli_num_rows($check) != 0){
		while($row = mysqli_fetch_assoc($check)){
			echo '<center>';
			echo '<div class="col-xl-3 col-md-6 col-sm-12">';
				echo '<div class="card" style="height: auto;">';
					echo '<div class="card-content">';
						echo '<div class="card-body">';
							echo '<p class="card-text">'.$row['post_content'].'</p>';
							echo '<text class="card-text">Posted By '.$row['post_by'].'</text>';
							echo '<br />';
							echo '<text class="card-text"> Posted On '.$row['post_date'].'</text>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';
			echo '</center>';
		}
	}
}else{
		echo "You must be logged in";
	}
?>