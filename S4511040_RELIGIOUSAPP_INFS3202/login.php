<!DOCTYPE html>
<html>
<head>
<link href="https://bootswatch.com/4/pulse/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
  <style>
    	.alert {
    		width: 500px;

    	}
    </style>
</head>
<body>

<div class="container">
	<br>  <p class="text-center">Religious App</p>
	<hr>
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
			<header class="card-header">
				<a href="register.php" class="float-right btn btn-outline-primary mt-1"> Register </a>
				<h4 class="card-title mt-2">Log In</h4>
			</header>
			<article class="card-body">
				<form method="POST" action="login.php">
					<div class="form-group">   
	  					<input type="text" name="username" class="form-control" placeholder="Username">
	  				</div>
	  				<div class="form-group">   
	  					<input type="password" name="password" class="form-control" placeholder="Password">
	  				</div>
		  			<div class="form-group">
		  				<button type="submit" name="submit" class="btn btn-outline-primary btn-block"> Login </button>
		  			</div>
		  			<div class="form-group">
		  				<a href="index.html"><button class="btn btn-outline-primary btn-block"> Go Back Home </a></button>
		  			</div>
				</form>
			</article>
			</div>
		</div>
	</div>
</div>

<center>
<?php
session_start();
include ('connect.php');
$username = @$_POST['username'];
$password = @$_POST['password'];
$sql = "SELECT * FROM db_user WHERE user_name='$username'";

if(isset($_POST['submit'])){
	if($username && $password){
		$check = mysqli_query($connect, $sql);
		$rows = mysqli_num_rows($check);

		if($rows != 0){
			while($row = mysqli_fetch_assoc($check)){
				$db_username = $row['user_name'];
				$db_password = $row['user_password'];
			}
			if($username == $db_username && password_verify($password, $db_password)){
				@$_SESSION['user_name'] = $username;
				header("Location: index1.php");
			}else{
				?>
				<br>
				<div class="alert alert-danger fade show mx-auto" role="alert">
					<strong>Password is wrong</strong>
				</div>
				<?php
			}
		}else{
			die("Could not find username");
		}
	}else{
		echo "Please fill in all the fields.";
	}
}
?>
</center>