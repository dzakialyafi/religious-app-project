<?php
  if(@$_SESSION["user_name"]){
?>
<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top" style="background-color: #340759;">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="index1.php">Religious App</a>
      <div class="navbar-collapse collapse" id="navbarCollapse" aria-expanded="false">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="account.php">Account</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php">Holybook</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="chaplaincy.php">Chaplaincy</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="prayer.php">Prayer Times</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="members.php">Members</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index1.php?action=logout">Logout</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">
              <?php
          $sql = "SELECT * FROM db_user WHERE user_name = '".$_SESSION['user_name']."'";
          $check = mysqli_query($connect, $sql);
          $rows = mysqli_num_rows($check);
          while ($row = mysqli_fetch_assoc($check)){
            $id = $row['user_id'];
          }
          echo "Logged in as ";
          echo @$_SESSION['user_name'];
        ?>
            </a>
          </li>
        </ul>
      </div>
    </nav>


<?php
}else{
  header("Location: login.php");
}
?>