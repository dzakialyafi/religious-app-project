<?php
  session_start();
  include ('connect.php');
  if(@$_SESSION["user_name"]){
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" contents="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<link rel="stylesheet" href="style_landing.css">
	<title>Welcome!</title>
</head>
<body>
	<div class="container">
    <div class="split left">
      <h1>I'm a Muslim</h1>
      <a href="quran_1.php" class="button">Let's Go!</a>
    </div>
  <div class="split right">
      <h1>I'm a Christian</h1>
      <a href="james1.php" class="button">Let's Go!</a>
  </div>
  </div>
<script src="landingpage.js"></script>
</body>
<?php
}else{
    echo 'You have logged out';
  }
?>