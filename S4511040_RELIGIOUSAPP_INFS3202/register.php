<!DOCTYPE html>
<html>
<head>
<link href="https://bootswatch.com/4/pulse/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
  <style>
    	.alert {
    		width: 500px;

    	}
    </style>
</head>
<body>

<div class="container">
	<br>  <p class="text-center">Religious App</p>
	<hr>
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
			<header class="card-header">
				<a href="login.php" class="float-right btn btn-outline-primary mt-1">Log in</a>
				<h4 class="card-title mt-2">Sign up</h4>
			</header>
			<article class="card-body">
				<form method="POST" action="register.php">
					<div class="form-group">   
	  					<input type="text" name="username" class="form-control" placeholder="Username">
	  				</div>
	  				<div class="form-group">   
	  					<input type="text" name="email" class="form-control" placeholder="Email">
	  				</div>
	  				<div class="form-group">   
	  					<input type="password" name="password" class="form-control" placeholder="Password">
	  				</div>
	  				<div class="form-group">   
		  				<input type="password" name="password2" class="form-control" placeholder="Re-Password">
		  			</div>
		  			<div class="form-group">
		  				<button type="submit" name="register_btn" class="btn btn-outline-primary btn-block"> Register </button>
		  			</div>
				</form>
			</article>
			</div>
		</div>
	</div>
</div>

<center>
<?php
session_start();
include ('connect.php');
$username = mysqli_real_escape_string($connect, @$_POST['username']);
$email = mysqli_real_escape_string($connect, @$_POST['email']);
$password = mysqli_real_escape_string($connect, @$_POST['password']);
$password2 = mysqli_real_escape_string($connect, @$_POST['password2']);
$verifiedpassword = password_hash($password, PASSWORD_BCRYPT);
$date_registered = date('Y/m/d');
$image = "images/bad-profile-pic-2.jpeg";
$sql = "INSERT INTO db_user(user_name, user_email, user_password, image, date_registered) VALUES ('$username','$email','$verifiedpassword', '$image', '$date_registered')";
$sql1 = "SELECT * FROM db_user WHERE user_name='$username'";

if(isset($_POST["register_btn"])){
	if($username && $email && $password && $password2){
		if(strlen($username) >= 5 && strlen($username) < 10 && strlen($password) > 6){
			$check = mysqli_query($connect, $sql1);
			if(mysqli_num_rows($check) == 0){
				if (filter_var($email, FILTER_VALIDATE_EMAIL)){
					if($password == $password2){
						if(mysqli_query($connect, $sql)){
								?>
								<br>
								<div class="alert alert-success fade show mx-auto" role="alert">
									You have been registered as <strong><?php echo $username; ?></strong> Click <a href='login.php'>here</a> to login
								</div>
								<?php
							}else{
								?>
								<br>
								<div class="alert alert-danger fade show mx-auto" role="alert">
									Failure. Please Try Again
								</div>
								<?php
							}
					}else{
						?>
						<br>
						<div class="alert alert-danger fade show mx-auto" role="alert">
							Password do not match
						</div>
						<?php
					}
				}else{
					?>
					<br>
					<div class="alert alert-danger fade show mx-auto" role="alert">
						Invalid Email format
					</div>
					<?php
			}
		}else{
				?>
				<br>
				<div class="alert alert-danger fade show mx-auto" role="alert">
					This user name has been used, please try another user name please
				</div>
				<?php
			}
		}else{
			if (strlen($username) < 5 || strlen($username) > 10){
			?>
			<br>
			<div class="alert alert-danger fade show mx-auto" role="alert">
				<strong>Username</strong> must be between 5 and 10 characters
			</div>
			<?php
			}

			if(strlen($password) < 6){
			?>
			<br>
			<div class="alert alert-danger fade show mx-auto" role="alert">
				<strong>Password</strong> must be longer than 6 characters
			</div>
			<?php
			}
		}
	}else{
		?>
		<br>
		<div class="alert alert-dark fade show mx-auto" role="alert">
			<strong>Please Fill in all the blank</strong>
		</div>
		<?php
	}
}
?>
</center>
</body>
</html>